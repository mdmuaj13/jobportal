<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;


Route::get('/', 'JobsController@index')->name('index');
Route::get('/post', 'JobsController@list')->name('job.list');
Route::get('/post/{id}', 'JobsController@show')->name('job.post');
Route::get('location/{loc}','JobsController@location')->name('job.location');
Route::get('category/{slug}','JobsController@category')->name('job.category');


Route::get('post-a-job','JobsController@create')->middleware('is_holder')->name('job.create');
Route::post('job-store','JobsController@store')->name('job.store');



Route::middleware('is_seeker')->group(function (){
    Route::post('apply_for_job', 'ApplyController@store')->name('job.apply');
    Route::get('apply/list','ApplyController@index')->name('job.apply.list');
    Route::post('wishlist/add','WishlistController@store')->name('job.wishlist.store');
    Route::get('wishlist', 'WishlistController@index')->name('job.wishlist.list');
    Route::post('wishlist/delete', 'WishlistController@destroy')->name('job.wishlist.destroy');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


