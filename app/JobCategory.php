<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    public function jobs(){
        return $this->hasMany(Jobs::class,'id','job_id');
    }

    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
}
