<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{

    public function categories(){
        return $this->hasOne(Category::class,'id','category');
    }
    public function jobCategory(){
        return $this->hasMany(JobCategory::class,'job_id','id');
    }
}
