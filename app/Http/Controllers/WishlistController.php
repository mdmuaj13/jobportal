<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request) {
        $uid  = Auth::user()->id;
        $prev = DB::select("SELECT id FROM wishlists WHERE user_id = $uid AND job_id = $request->job_id ");
        if($prev){
            return redirect()->back()->with(
                'error' , 'Already added to your Wishlist!!'
            );
        }else {
            $apply = New Wishlist();
            $apply->user_id = Auth::user()->id;
            $apply->job_id = $request->job_id;
            $apply->save();

            return redirect()->back()->with(
                'success' , 'Successfully added to your Wishlist...'
            );
        }


    }


    public function show(Wishlist $wishlist)
    {
        //
    }


    public function edit(Wishlist $wishlist)
    {
        //
    }


    public function update(Request $request, Wishlist $wishlist)
    {
        //
    }

    public function destroy(Wishlist $wishlist)
    {
        //
    }
}
