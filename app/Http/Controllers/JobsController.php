<?php

namespace App\Http\Controllers;

use App\Category;
use App\JobCategory;
use App\Jobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $jobs = Jobs::inRandomOrder()->take(3)->get();
        $locations = DB::select("select distinct location, count(location) as count from `jobs` group by location");
        $categories = Category::inRandomOrder()->take(5)->get();

        return view('frontend.home')->with([
            'jobs' =>$jobs,
            'locations' => $locations,
            'categories' => $categories,
        ]);
    }


    public function create()
    {
        $categories = Category::all();

        return view('frontend.jobpost')->with([
           'categories' => $categories
        ]);
    }


    public function store(Request $request) {
        $job = New Jobs();

        $job->client_id= $request->client_id;
        $job->title= $request->title;
        $job->description= $request->description;
        $job->requirement= $request->requirement;
        $job->experience= $request->experience;
        $job->nature= $request->nature;
        $job->salary= $request->salary;
        $job->vacancy= $request->vacancy;
        $job->compensation= $request->compensation;
        $job->age= $request->age;
        $job->role= $request->role;
        $job->category = $request->category;
        $job->company= $request->company;
        $job->location= $request->location;
        $job->deadline= $request->deadline;

        $job->save();

        $jobid = $job->id;

        $cat = new JobCategory();
        $cat->job_id = $jobid;
        $cat->category_id = $request->category;
        $cat->save();

        return redirect()->back()->with('success','Job created successfully..');

    }

    public function location($loc){
        $jobs = Jobs::where('location',$loc)->paginate(3);

        $locations = DB::select("select distinct location, count(location) as count from `jobs` group by location");
        $categories = Category::inRandomOrder()->take(5)->get();

        return view('frontend.list')->with([
            'jobs' => $jobs,
            'locations' => $locations,
            'categories' => $categories,
        ]);
    }
    public function category($slug){
        $cat = Category::select('id')->where('slug',$slug)->first();

        $jobs = Jobs::where('category',$cat)->paginate(3);

        $locations = DB::select("select distinct location, count(location) as count from `jobs` group by location");
        $categories = Category::inRandomOrder()->take(5)->get();

        return view('frontend.list')->with([
            'jobs' => $jobs,
            'locations' => $locations,
            'categories' => $categories,
        ]);
    }


    public function list(){
        $jobs = Jobs::paginate(3);
        $locations = DB::select("select distinct location, count(location) as count from `jobs` group by location");
        $categories = Category::inRandomOrder()->take(5)->get();

        return view('frontend.list')->with([
            'jobs' => $jobs,
            'locations' => $locations,
            'categories' => $categories,
        ]);
    }

    public function show($id) {
        $job = Jobs::with('categories')->find($id);
        $cat = JobCategory::with('category')->where('job_id',$id)->first();
//        dd($cat);
        $categories = Category::inRandomOrder()->take(5)->get();
        $locations = DB::select("select distinct location, count(location) as count from `jobs` group by location");

        return view('frontend.single')->with([
            'job' => $job,
            'locations' => $locations,
            'categories' => $categories,
            'cat' => $cat
        ]);
    }


    public function edit(Jobs $jobs)
    {
        //
    }


    public function update(Request $request, Jobs $jobs)
    {
        //
    }


    public function destroy(Jobs $jobs)
    {
        //
    }
}
