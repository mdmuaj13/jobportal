<?php

namespace App\Http\Controllers;

use App\Apply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\New_;

class ApplyController extends Controller
{
    public function __construct() {
        $this->middleware('is_seeker');

    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request) {
        $uid  = Auth::user()->id;
        $prev = DB::select("SELECT id FROM applies WHERE user_id = $uid AND job_id = $request->job_id ");
        if($prev){
            return redirect()->back()->with(
                'error' , 'Already applied for this job'
            );
        }else {
            $apply = New Apply();
            $apply->user_id = Auth::user()->id;
            $apply->job_id = $request->job_id;
            $apply->save();

            return redirect()->back()->with(
                'success' , 'You successfully applied for job...'
            );
        }


    }


    public function show(Apply $apply) {
        //
    }


    public function edit(Apply $apply)
    {
        //
    }


    public function update(Request $request, Apply $apply)
    {
        //
    }


    public function destroy(Apply $apply)
    {
        //
    }
}
