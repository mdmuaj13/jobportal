<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null(Auth::user())) {
            if (auth()->user()->isAdmin()) {
                return $next($request);
            }
        } else {
            return redirect('login')->with('error','You need to log first!');
        }

        return redirect()->back()->with(
            'error', "Is admin"
        );
    }
}
