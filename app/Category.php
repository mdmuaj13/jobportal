<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public  function  jobCategory(){
        return $this->belongsTo(JobCategory::class,'category_id','id');
    }
    public  function  job(){
        return $this->belongsTo(Jobs::class);
    }
}
