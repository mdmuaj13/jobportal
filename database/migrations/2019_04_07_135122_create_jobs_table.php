<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('requirement')->nullable();
            $table->string('experience')->nullable();
            $table->string('nature')->nullable();
            $table->string('salary')->default('negotiable');
            $table->integer('vacancy')->default(1);
            $table->string('compensation')->nullable();
            $table->string('age')->nullable();
            $table->string('role')->nullable();
            $table->string('category');
            $table->string('company');
            $table->string('location')->nullable();
            $table->date('deadline')->nullable();

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
