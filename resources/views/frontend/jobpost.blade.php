@extends('theme.master')

@section('title', 'Post a job')


@section('content')


    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Post A Job
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a>
                        <span class="lnr lnr-arrow-right"></span>  <a href="about-us.html"> Post A Job </a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="post-area">
        <div class="container">
        <div class="section-top-border">
            <div class="row">
                <div class="col-lg-9 col-md-10">
                    <h3 class="mb-30">New Job Post</h3>
                    <form action="{{ route('job.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="client_id" value="1"  required class="single-input-primary">

                        <div class="mt-10">
                            <input type="text" name="title" placeholder="Job Title"  required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <textarea class="single-textarea" name="description" placeholder="Description"  required></textarea>
                        </div>
                        <div class="mt-10">
                            <textarea class="single-textarea" name="requirement" placeholder="Requirements"  required></textarea>
                        </div>
                        <div class="mt-10">
                            <textarea class="single-textarea" name="experience" placeholder="Experience"  required></textarea>
                        </div>
                        <div class="mt-10">
                            <input type="text" name="company" placeholder="Company Name"   required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="role" placeholder="Job Role"   required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="salary" placeholder="Salary Range"   required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="compensation" placeholder="Compensation"   required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="age" placeholder="Age"   required class="single-input-primary">
                        </div>
                        <div class="mt-10">
                            <input type="text" name="vacancy" placeholder="Vacancy"   required class="single-input-primary">
                        </div>
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                            <div class="form-select" id="default-select">
                            <select name="nature" required>
                                <option >Select Job Nature</option>
                                <option value="part">Part Time</option>
                                <option value="full">Full Time</option>
                                <option value="intern">Intern</option>
                            </select>
                        </div>
                        </div>


                <div class="input-group-icon mt-10">
                    <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                    <div class="form-select" id="default-select">
                    <select name="location">
                        <option >Select Location </option>
                        <option value="dhaka">Dhaka</option>
                        <option value="chattagram">Chattagram</option>
                        <option value="rajshahi">Rajshahi</option>
                        <option value="khulna">Khulna</option>
                        <option value="barishal">Barishal</option>
                        <option value="sylhet">Sylhet</option>
                    </select>
                    </div>
                </div>
                <div class="input-group-icon mt-10">
                    <div class="icon"><i class="fa fa-map-pin" aria-hidden="true"></i></div>
                    <div class="form-select " id="default-select">
                        <select name="category" multiple data-live-search="true" required>
                            <option value="" disabled selected>Job Category</option>
                            @foreach($categories as $cat)
                                <option value="{{ $cat['id'] }}">{{ $cat['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="mt-10">
                    <div class="row">
                        <p class="col-md-2 icon"><label for="deadline">Deadline</label> </p>
                        <input type="date" id="deadline" name="deadline" placeholder="Late date of Apply"   required class="single-input-primary col-md-10">

                    </div>
                </div>

            <div class="mt-20 text-center">
                <input type="submit" class=" btn btn-primary" name="s" value="Post Job">
            </div>

            </form>
        </div>

            </div>
        </div>
        </div>


    </section>

@endsection