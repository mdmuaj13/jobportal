@extends('theme.master')

@section('title',$job['title'] )

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Job Details
                    </h1>
                    <p class="text-white link-nav"><a href="{{ route('index') }}">Home </a>
                        <span class="lnr lnr-arrow-right"></span>  <a href="{{ route('job.list') }}"> Jobs</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start post Area -->
    <section class="post-area section-gap">
        <div class="container">
            <div class="row justify-content-center d-flex">
                <div class="col-lg-8 post-list">
                    <div class="single-post d-flex flex-row">
                        <div class="thumb pr-2">
                            <img src="{{ asset('frontend/img/post.png') }} " alt="">
                            <ul class="tags">
                                <li>
                                    <a href="#"> {{ $job->category}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="details">
                            <div class="title d-flex flex-row justify-content-between">
                                <div class="titles">
                                    <h4>{{ $job->title }}</h4>
                                    <h6> {{ $job->company }}</h6>
                                </div>
                                <ul class="btns">
                                    <li><a href="{{ route('job.wishlist.store') }}"
                                           onclick="event.preventDefault(); document.getElementById('wishlist').submit();">
                                            <span class="lnr lnr-heart"></span></a></li>
                                    <li><a href="{{ route('job.apply') }}"
                                           onclick="event.preventDefault(); document.getElementById('jobapply').submit();">
                                            Apply </a></li>
                                    <form id="jobapply" action="{{ route('job.apply') }}" method="POST" style="display: none">
                                        @csrf
                                        <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    </form>
                                    <form id="wishlist" action="{{ route('job.wishlist.store') }}" method="POST" style="display: none">
                                        @csrf
                                        <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    </form>
                                </ul>
                            </div>
                            <p>
                                {{ str_limit($job['description'], $limit= 100, $end = ' ....') }}
                            </p>
                            <h5>Job Nature: {{ $job['nature'] }}</h5>
                            <p class="address"><span class="lnr lnr-map"></span> {{ $job['location'] }}</p>
                            <p class="address"><span class="lnr lnr-database"></span> {{ $job['salary'] }}</p>
                        </div>
                    </div>
                    <div class="single-post job-details">
                        <h4 class="single-title">Whom we are looking for</h4>
                        <p>
                            {{ $job['description'] }}

                        </p>

                    </div>
                    <div class="single-post job-experience">
                        <h4 class="single-title">Company</h4>
                        <ul>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Name:  <span> {{ $job['company'] }} </span>
                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Location:  <span> {{ $job['location'] }} </span>
                            </li>

                        </ul>
                    </div>
                    <div class="single-post job-experience">
                        <h4 class="single-title">Experience Requirements</h4>
                        <ul>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Experience:  <span> {{ $job['experience'] }} </span>
                            </li>

                        </ul>
                    </div>
                    <div class="single-post job-experience">
                        <h4 class="single-title">Education Requirements</h4>
                        <ul>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                  <span> {{ $job['requirement'] }} </span>
                            </li>
                        </ul>
                    </div>
                    <div class="single-post job-experience">
                        <h4 class="single-title">Job Features & Overviews</h4>
                        <ul>

                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Vacancy:  <span> {{ $job['vacancy'] }} </span>
                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Salary :
                                <span> {{ $job['salary'] }} </span>
                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Age :
                                <span> {{ $job['age'] }} </span>
                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Role :
                                <span> {{ $job['role'] }} </span>

                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Compensation :
                                <span> {{ $job['compensation'] }} </span>
                            </li>
                        </ul>
                    </div>

                    <div class="single-post job-experience">
                        <h4 class="single-title">Job Dates</h4>
                        <ul>

                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Last date of apply :
                                <span> {{ \Carbon\Carbon::parse($job['deadline'])->format('d/m/Y') }} </span>
                            </li>
                            <li>
                                <img src=" {{ asset('frontend/img/pages/list.jpg' ) }}" alt="">
                                Job Posted :
                                <span> {{ \Carbon\Carbon::parse($job['created_at'])->format('d/m/Y') }} </span>
                            </li>
                        </ul>
                    </div>

                </div>
                @include('theme.includes.sidebar')
            </div>
        </div>
    </section>
    <!-- End post Area -->

@endsection
