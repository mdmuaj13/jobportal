@extends('theme.master')

@section('title', 'Category')

@section('content')


    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Job category
                    </h1>
                    <p class="text-white link-nav"><a href="{{ route('index') }}">Home </a>
                        <span class="lnr lnr-arrow-right"></span>  <a href="{{ route('job.list') }}"> Jobs</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start post Area -->
    <section class="post-area section-gap">
        <div class="container">
            <div class="row justify-content-center d-flex">
                <div class="col-lg-8 post-list">
                    @foreach($jobs as $job)
                        <div class="single-post d-flex flex-row">
                            <div class="thumb mr-2">
                                <img src="{{ asset('frontend/img/post.png') }}" alt="">

                            </div>
                            <div class="details">
                                <div class="title d-flex flex-row justify-content-between">
                                    <div class="titles">
                                        <a href="{{ route('job.post', ['id' => $job['id'] ]) }}"><h4>{{ $job['title'] }}</h4></a>
                                        <h6> {{ $job['company'] }}</h6>
                                    </div>
                                    <ul class="btns">
                                        <li><a href="{{ route('job.wishlist.store') }}"
                                               onclick="event.preventDefault(); document.getElementById('wishlist').submit();">
                                                <span class="lnr lnr-heart"></span></a></li>
                                        <li><a href="{{ route('job.apply') }}"
                                               onclick="event.preventDefault(); document.getElementById('jobapply').submit();">
                                                Apply </a></li>
                                        <form id="jobapply" action="{{ route('job.apply') }}" method="POST" style="display: none">
                                            @csrf
                                            <input type="hidden" name="job_id" value="{{ $job->id }}">
                                        </form>
                                        <form id="wishlist" action="{{ route('job.wishlist.store') }}" method="POST" style="display: none">
                                            @csrf
                                            <input type="hidden" name="job_id" value="{{ $job->id }}">
                                        </form>
                                    </ul>
                                </div>
                                <p>
                                    {{ str_limit($job['description'], $limit= 100, $end = ' ....') }}
                                </p>
                                <h5>Job Nature: {{ $job['nature'] }}</h5>
                                <p class="address"><span class="lnr lnr-map"></span> {{ $job['location'] }}</p>
                                <p class="address"><span class="lnr lnr-database"></span> {{ $job['salary'] }}</p>
                            </div>
                        </div>
                    @endforeach

                    {{ $jobs->links() }}

                </div>
                @include('theme.includes.sidebar')
            </div>
        </div>
    </section>
    <!-- End post Area -->

@endsection