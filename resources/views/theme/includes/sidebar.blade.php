<div class="col-lg-4 sidebar">
    <div class="single-slidebar">
        <h4>Jobs by Location</h4>
        <ul class="cat-list">
            @foreach($locations as $loc)
            <li><a class="justify-content-between d-flex" href="{{ route('job.location', $loc->location) }}"><p>{{ $loc->location }}</p><span>{{ $loc->count }}</span></a></li>

            @endforeach
        </ul>
    </div>


    <div class="single-slidebar">
        <h4>Jobs by Category</h4>
        <ul class="cat-list">
            @foreach($categories as $cat)
                <li><a class="justify-content-between d-flex" href="{{ route('job.category', $cat->slug) }}"><p>{{ $cat['name'] }}</p></a></li>
            @endforeach
        </ul>
    </div>



</div>