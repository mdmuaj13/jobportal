<header id="header" id="home">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="{{ route('index') }}"><img src="{{ asset('frontend/img/logo.png') }} " alt="" title="" /></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="{{ route('index') }}">Home</a></li>
                    <li><a href="{{ route('job.list') }}">Jobs</a></li>
                    <li><a href="">About Us</a></li>
                    <li><a href="">Contact</a></li>

                    @guest
                    <li><a class="ticker-btn" href="{{ route('register') }}">Signup</a></li>
                    <li><a class="ticker-btn" href="{{ route('login') }}">Login</a></li>
                    @else
                        <li class="menu-has-children"><a href="#">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                            <ul>
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Wishlist</a></li>
                                <li><a href="{{ route('logout') }}"   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header><!-- #header -->

@if(session()->has('success'))
    <br><br>
    <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
        <h4> {{session()->get('success')}}</h4>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

@elseif(session()->has('error'))
    <br><br>
    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
        <h4> {{session()->get('error')}}</h4>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif(session()->has('alert'))
    <br><br>
    <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
        <h4> {{session()->get('info')}}</h4>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

