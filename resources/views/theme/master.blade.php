<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    @include('theme.includes.header')
</head>
<body>

@include('theme.includes.navbar')

@yield('content')


<!-- start footer Area -->
@include('theme.includes.footer')
<!-- End footer Area -->

@include('theme.includes.script')


</body>
</html>



